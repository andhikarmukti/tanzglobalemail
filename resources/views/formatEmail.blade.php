<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- Tailwind CSS --}}
    <link rel="stylesheet" href="/css/tailwindstyle.css">
    <title>Email</title>
</head>
<body>
    <div class="flex flex-col gap-1">
        <h3>Name : {{ $mailData['name'] }}</h3>
        <h3>Email : {{ $mailData['email'] }}</h3>
        <h3>subject : {{ $mailData['subject'] }}</h3>
    </div>
    <br><hr>
    <p>{{ $mailData['message'] }}</p>






    <script src="https://unpkg.com/flowbite@1.5.2/dist/flowbite.js"></script>
</body>
</html>
