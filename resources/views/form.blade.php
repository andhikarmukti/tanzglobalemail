<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    {{-- Tailwind CSS --}}
    {!! ReCaptcha::htmlScriptTagJsApi() !!}
    <script src="https://code.jquery.com/jquery-3.6.1.js" integrity="sha256-3zlB5s2uwoUzrXK3BT7AX3FyvojsraNFxCc2vC/7pNI=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="/css/tailwindstyle.css">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Email</title>
</head>
<body>
    <div class="flex justify-center overscroll-x-none">
        <div class="border border-slate-300 rounded-lg shadow-md p-5 w-full">
            <form target="my_iframe">
                <div class="mb-6">
                    <label for="name" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Your name</label>
                    <input type="name" id="name" name="name" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                    @error('name')
                        <small class="text-red-500 italic">{{ $message }}</small>
                    @enderror
                  </div>
                <div class="mb-6">
                  <label for="email" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Your email</label>
                  <input type="email" id="email" name="email" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                    @error('email')
                        <small class="text-red-500 italic">{{ $message }}</small>
                    @enderror
                </div>
                <div class="mb-6">
                  <label for="subject" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Subject</label>
                  <input type="text" id="subject" name="subject" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" autocomplete="off">
                    @error('subject')
                        <small class="text-red-500 italic">{{ $message }}</small>
                    @enderror
                </div>
                <div class="mb-6">
                    <label for="message" class="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Your message</label>
                    <textarea id="message" name="message" rows="4" class="block p-2.5 w-full text-sm text-gray-900 bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Write your message here..."></textarea>
                    @error('message')
                        <small class="text-red-500 italic">{{ $message }}</small>
                    @enderror
                </div>
                <div class="mb-6">
                    <div class="flex justify-center" id="g-recaptcha-response"> {!! htmlFormSnippet() !!} </div>
                    @error('g-recaptcha-response')
                        <small class="text-red-500 italic">{{ $message }}</small>
                    @enderror
                </div>
                <button id="buttonSubmit" type="button" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
                <br>
                <small id="sending" class="text-green-500 italic" hidden>Sending email.. Please wait..</small>
                <small id="sendingFail" class="text-red-500 italic" hidden>Failed to send email</small>
                {{-- @if(session()->has('message')) --}}
                <div id="alertSuccess" class="alert alert-success mt-6" hidden>
                    <div id="alert-1" class="flex p-4 mb-4 bg-green-100 rounded-lg dark:bg-green-200" role="alert">
                        <svg aria-hidden="true" class="flex-shrink-0 w-5 h-5 text-green-700 dark:text-green-800" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-7-4a1 1 0 11-2 0 1 1 0 012 0zM9 9a1 1 0 000 2v3a1 1 0 001 1h1a1 1 0 100-2v-3a1 1 0 00-1-1H9z" clip-rule="evenodd"></path></svg>
                        <span class="sr-only">Info</span>
                        <div class="ml-3 text-sm font-medium text-green-700 dark:text-green-800">
                            Email is sent successfully
                        </div>
                            <button type="button" class="ml-auto -mx-1.5 -my-1.5 bg-green-100 text-green-500 rounded-lg focus:ring-2 focus:ring-green-400 p-1.5 hover:bg-green-200 inline-flex h-8 w-8 dark:bg-green-200 dark:text-green-600 dark:hover:bg-green-300" data-dismiss-target="#alert-1" aria-label="Close">
                            <span class="sr-only">Close</span>
                            <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                        </button>
                    </div>
                </div>
                {{-- @endif --}}
            </form>
        </div>
    </div>


    <script src="https://unpkg.com/flowbite@1.5.2/dist/flowbite.js"></script>
    <script>
        function removeDisable(){
            $('#name').removeAttr('disabled');
            $('#email').removeAttr('disabled');
            $('#subject').removeAttr('disabled');
            $('#message').removeAttr('disabled');
            $('#g-recaptcha-response').removeAttr('disabled');
            $('#buttonSubmit').removeAttr('disabled');
        }
        function disableInput(){
            $('#name').attr('disabled', true);
            $('#email').attr('disabled', true);
            $('#subject').attr('disabled', true);
            $('#message').attr('disabled', true);
            $('#g-recaptcha-response').attr('disabled', true);
            $('#buttonSubmit').attr('disabled', true);
        }

        $('#buttonSubmit').click(function(){
            const name = $('#name').val();
            const email = $('#email').val();
            const subject = $('#subject').val();
            const message = $('#message').val();
            const recaptcha = $('#g-recaptcha-response').val();

            disableInput();

            $('#sending').removeAttr('hidden');

            $.ajax({
                url : '/email',
                method : 'POST',
                // headers: {
                //     'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                // },
                datatype: 'json',
                data : {
                    // "_token" : "{{ csrf_token() }}",
                    name : name,
                    email : email,
                    subject : subject,
                    message : message,
                    recaptcha : recaptcha
                },
                success : function(res){
                    if(res.success == true){

                        $('#name').val('');
                        $('#email').val('');
                        $('#subject').val('');
                        $('#message').val('');
                        $('#g-recaptcha-response').val('');
                        $('#buttonSubmit').val('');

                        $('#sending').attr('hidden', true);
                        $('#alertSuccess').removeAttr('hidden');

                        setTimeout(() => {
                            window.location.reload()
                        }, 3000);
                    }
                },
                error : function(error){
                    error = error.responseJSON;
                    $('#sendingFail').removeAttr('hidden');
                    $('#sending').attr('hidden', true);
                    setTimeout(() => {
                        window.location.reload()
                    }, 2000);
                }
            });
        });
    </script>
</body>
</html>
