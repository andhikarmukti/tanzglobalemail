<?php

namespace App\Http\Controllers;

use App\Mail\SendMail;
use App\Jobs\SendMailJob;
use App\Models\FormEmail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreFormEmailRequest;
use App\Http\Requests\UpdateFormEmailRequest;

class FormEmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('form');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreFormEmailRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request->all());
        $rules = [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'subject' => 'required',
            'message' => 'required',
            'recaptcha' => 'recaptcha',
        ];
        $message = [
            'recaptcha' => 'google recaptcha invalid!'
        ];
        $validator = Validator::make($request->all(), $rules, $message);
        if($validator->fails()){
            // return redirect('https://tanzemail.artapuri.com/')->withErrors($validator->errors());
            return response()->json([
                'success' => false,
                'data' => $validator->errors()
            ], 400);
        }

        $mailData = [
            'name' => $request->name,
            'email' => $request->email,
            'subject' => $request->subject,
            'message' => $request->message,
        ];

        // dispatch(new SendMailJob($mailData));
        Mail::to('info@tanzglobal.com')->send(new SendMail($mailData));
        // return redirect('https://tanzemail.artapuri.com/')->with('success', 'Email is sent successfully.');
        return response()->json([
            'success' => true,
            'data' => 'Email is sent successfully.'
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\FormEmail  $formEmail
     * @return \Illuminate\Http\Response
     */
    public function show(FormEmail $formEmail)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\FormEmail  $formEmail
     * @return \Illuminate\Http\Response
     */
    public function edit(FormEmail $formEmail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateFormEmailRequest  $request
     * @param  \App\Models\FormEmail  $formEmail
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFormEmailRequest $request, FormEmail $formEmail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\FormEmail  $formEmail
     * @return \Illuminate\Http\Response
     */
    public function destroy(FormEmail $formEmail)
    {
        //
    }
}
